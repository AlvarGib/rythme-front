Dado el nombre de una clase como String, 
crea una nueva instancia de la clase sin usar el operador "new"
```
class Persona {
 saluda() {
 return console.log('Hola me llamo pepe')
 }
}
 
window.Persona = Persona;
 
const classname = 'Persona';
 
var instance;
```
 
Crea la instancia en la variable 'instance'