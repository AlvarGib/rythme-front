import React from "react";
import { Link, Redirect } from "react-router-dom";
import { motion } from "framer-motion";
import { useSelector } from "react-redux";
import { selectHasUser, selectUser } from '../../reducers/userSlice';
import logo from '../../RithmeOk.png'

import "./Login.scss"
const Login = props => {
  const user = useSelector(selectUser);
  const hasUser = useSelector(selectHasUser);

  if(hasUser === true){
    return <Redirect to="/home" />;
  }else{
  return (
    <motion.div className="login"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
    >
      <motion.h2 className="login__logo t-shadow-halftone"
        animate={{ y: -100 }}
        transition={{ type: 'spring', stiffness: 120 }}
      >
      <img src={logo} width="320px" height="160px" alt="" />
      </motion.h2>
      {user.user === null && 
      <div className="login__container">
        <div>
          <Link to="/login" className="login__container__link">Iniciar sesión</Link>
        </div>
      </div>}
      {user.user === null &&
      <div>
        <Link to="/register" className="login__container__link">Registrarme</Link>
      </div>
      }
    </motion.div>
  );
  }
}


export default Login;
