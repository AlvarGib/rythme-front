import React from "react";

import Lottie from "react-lottie";
import logo from '../../RithmeOk.png'

import * as loading from "../../loading2.json";

import "./InitialLoading.scss"
const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};

const InitialLoading = () => {
    return (
        <div className="wrapperLoading">
            <img src={logo} alt="logo" className="wrapperLoading__img"/>
            <Lottie classname="wrapperLoading__lottie" options={defaultOptions} width="340px" height="148px" />
            {/* <h2 className="wrapperLoading__text">Rith<span><img src={MusicColor} alt="" className="wrapperLoading__text__img" /></span><div className="e">e</div></h2> */}
        </div>
    )
}
export default InitialLoading;