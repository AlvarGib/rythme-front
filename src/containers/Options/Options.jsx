import React from 'react'
import { withRouter } from "react-router-dom";
import { Link, Redirect } from "react-router-dom";
import { logoutUser, selectHasUser } from "../../reducers/userSlice"

import arrow from "../../icons/arrow/rigth/next.svg"

import "./Options.scss"
import { useDispatch, useSelector } from 'react-redux';

const Options = () => {
    const dispatch = useDispatch();
    const hasUser = useSelector(selectHasUser);

    if(hasUser === false){

    return <Redirect to="/login" />;
    }
    
    return (
        <div className="options">
            <div className="options--text">
                <p>Personal</p>
            </div>
            <button className="options--btn rounded">
                <Link to="/settings/profile" className="links">
                    <span className="text">
                        Editar perfil
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <button className="options--btn rounded">
                <Link to="/settings/calendar" className="links">
                    <span className="text">
                        Sincronizar calendario
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <button className="options--btn rounded">
                <Link to="/chat" className="links">
                    <span className="text">
                        Notificaciones
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <button className="options--btn rounded">
                <Link to="/settings/map" className="links">
                    <span className="text">
                        Ubicación
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <button className="options--btn rounded">
                <Link to="/mytickets" className="links">
                    <span className="text">
                        Historial de compras
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <div className="options--text">
                <p>Sincronizar preferencias</p>
            </div>
            <button className="options--btn rounded">
                <Link to="/streaming" className="links">
                    <span className="text">
                        Sincroniza tu música
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <button className="options--btn rounded">
                <Link to="/rooms" className="links">
                    <span className="text">
                        Sincroniza tus salas favoritas
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <div className="space"> </div>
            <button className="options--btn rounded">
                <Link to="/help" className="links">
                    <span className="text">
                        Ayuda
                </span>
                </Link>
                <img src={arrow} className="options--image" alt="" />
            </button>
            <button onClick={() => dispatch(logoutUser())} className="options--btn rounded">
                    <span className="text">
                        Cerrar sesión
                </span>
                <img src={arrow} className="options--image" alt="" />
            </button>
        </div>
    )
}

export default withRouter(Options);
