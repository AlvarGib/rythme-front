import { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useSelector, useDispatch, } from "react-redux";
import Login from "./pages/Login";
import RegisterForm from "./pages/RegisterForm";
import LoginForm from "./pages/LoginForm";
import RecoverForm from "./pages/RecoverForm/RecoverForm";
import SecureRoute from "./components/SecureRoute";
import InitialLoading from "./containers/InitialLoading/InitialLoading";
import Navbar from "./components/Navbar";
import UserProfile from "./pages/UserProfile/UserProfile";
import EditProfile from "./pages/FormEditProfile/EditProfile";


import Home from "./containers/Home"
import Header from "./components/Header";
import Options from "./containers/Options";
import Concerts from "./containers/Concerts";
import ConcertDetail from "./containers/ConcertDetail/ConcertDetail";
import ConcertTicket from "./containers/ConcertTicket";
import Payment from "./containers/Payment";
import ConfirmingBuy from "./components/ConfirmingBuy";
import Streaming from "./components/Streaming";
import BuyLoading from "./components/BuyLoading";
import CalendarRytme from "./components/CalendarRytme";
import MyConcerts from "./containers/MyConcerts/MyConcerts";
import Ubication from "./components/Map";
import Search from "./components/Search/Search";
import Chat from "./containers/Chat/Chat";
import FanClub from "./containers/FanClub/FanClub";
import Rooms from "./components/Roms";
import Help from "./components/Help";

import {
  selectHasUser,
  checkUserSession,
} from "./reducers/userSlice";
import "./App.scss";





const App = (props) => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);
  const hasUser = useSelector(selectHasUser);


  useEffect(() => {
    dispatch(checkUserSession());
    setTimeout(() => {
      setLoading(false)
    }, 2000);
  }, [dispatch]);


  if (loading) {
    return (
      <InitialLoading />
    )
  }

  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route
            path="/register"
            component={(props) => (
              <RegisterForm {...props} />
            )}
          />
          <Route
            path="/login/recover"
            component={(props) => (
              <RecoverForm {...props} />
            )}
          />
          <Route
            path="/login"
            component={(props) => (
              <LoginForm {...props} />
            )}
          />
          <SecureRoute
            path="/home"
            hasUser={hasUser}
            component={(props) => <Home {...props} />}
          />
          <SecureRoute
            path="/settings/profile/edit"
            hasUser={hasUser}
            component={(props) => <EditProfile {...props} />}
          />
          <SecureRoute
            path="/settings/profile"
            hasUser={hasUser}
            component={(props) => <UserProfile {...props} />}
          />
          <SecureRoute
            path="/settings/calendar"
            hasUser={hasUser}
            component={(props) => <CalendarRytme {...props} />}
          />
          <SecureRoute
            path="/settings/map"
            hasUser={hasUser}
            component={(props) => <Ubication {...props} />}
          />
          <SecureRoute
            path="/settings"
            hasUser={hasUser}
            component={(props) => <Options {...props} />}
          />
          <SecureRoute
            path="/ticket" exact
            hasUser={hasUser}
            component={(props) => <Concerts {...props} />}
          />
          <SecureRoute path="/ticket/:id" exact
            hasUser={hasUser}
            component={(props) => <ConcertDetail {...props} />}
          />
          <SecureRoute path="/ticket/:id/buy-ticket"
            hasUser={hasUser}
            component={(props) => <ConcertTicket {...props} />}
          />
          <SecureRoute path="/ticket/:id/pago"
            hasUser={hasUser}
            component={(props) => <Payment {...props} />}
          />
          <SecureRoute path="/youticket"
            hasUser={hasUser}
            component={(props) => <ConfirmingBuy {...props} />}
          />
          <SecureRoute path="/mytickets"
            hasUser={hasUser}
            component={(props) => <MyConcerts {...props} />}
          />
          <SecureRoute
            path="/streaming" exact
            hasUser={hasUser}
            component={(props) => <Streaming {...props} />}
          />
          <Route
            path="/loadingbuy" exact
            hasUser={hasUser}
            component={(props) => <BuyLoading {...props} />}
          />
          <Route
            path="/search" exact
            hasUser={hasUser}
            component={(props) => <Search {...props} />}
          />
          <Route
            path="/chat" exact
            hasUser={hasUser}
            component={(props) => <Chat {...props} />}
          />
          <Route
            path="/fanclub" exact
            hasUser={hasUser}
            component={(props) => <FanClub {...props} />}
          />
          <Route
            path="/rooms" exact
            hasUser={hasUser}
            component={(props) => <Rooms {...props} />}
          />
          <Route
            path="/help" exact
            hasUser={hasUser}
            component={(props) => <Help {...props} />}
          />
          <Route
            path="/"
            component={(props) => <Login {...props}
            />}
          />
        </Switch>
      </div>
      <div className="navbar">
        <Navbar />
      </div>
    </Router>
  );
};

export default App;
