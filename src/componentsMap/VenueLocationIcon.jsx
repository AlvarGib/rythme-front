import L from "leaflet";

export const VenueLocationIcon = L.icon({
  iconUrl: require("../icons/miscelania/gps.svg"),
  iconRetinaUrl: require("../icons/miscelania/gps.svg"),
  iconAnchor: [35, 35],
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [35, 35],
  className: "leaflet-venue-icon",
});

export default VenueLocationIcon