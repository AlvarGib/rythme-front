import { createSlice } from '@reduxjs/toolkit';


const ticketUrl = "https://rithme.herokuapp.com/ticket/create";
const myticketUrl = "https://rithme.herokuapp.com/auth/tickets";

export const ticketSlice = createSlice({
    name: 'ticket',
    initialState: {
        hasUser: false,
        ticket: null,
        error: null,
        myTicket:null
    },
    reducers: {

        setTicket: (state, action) => {

            state.ticket = action.payload;
        },
        setError: (state, action) => {

            state.error = action.payload;
        },
        hasUser: (state, action) => {
            state.hasUser = action.payload;
        },
        setMyTicket: (state, action) => {
            state.myTicket = action.payload;
        }
    },
});

const { setTicket, setError, hasUser, setMyTicket } = ticketSlice.actions;

export const myTicketList = () => async dispatch => {
    const request = await fetch(myticketUrl, {
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        credentials: 'include',
    });

    const response = await request.json();
    if (!request.ok) {
        dispatch(setError(response))
    } else {
        dispatch(setTicket(response));
        dispatch(hasUser(true))

    }

};

export const registerTicket = ({ form, c_back }) => async dispatch => {
    const request = await fetch(ticketUrl, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Access-Control-Allow-Credentials': true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(form),
    })

    const response = await request.json();

    if (!request.ok) {

        dispatch(setError(response));
    } else {
        dispatch(setTicket(response));
        c_back();
    }
};


export const selectHasUser = state => state.ticket.hasUser;
export const selectTicket = state => state.ticket;
export const selectMyTicket = state => state.ticket.myTicket;
// export const selectError = state => state.ticket.error;

export default ticketSlice.reducer;