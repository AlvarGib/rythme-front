import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { selectHasUser } from '../../reducers/userSlice';


const SecureRoute = (props) => {
    const hasUser = useSelector(selectHasUser);
    const { ...restProps } = props;
    if (hasUser === null) {
        return (
            <div>Cargando...</div>
        )
    } else {
        return hasUser ? <Route {...restProps} /> : <Redirect to="/login" />
    }
}

export default SecureRoute;