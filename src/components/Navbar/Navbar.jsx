import React, { useState } from 'react'

import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

import Tickets from "../../icons/tickets/tickets.svg";
import TicketColor from "../../icons/tickets/TicketColor.png";

import Home from "../../icons/star/star.svg";
import HomeColor from "../../icons/star/starcolor.svg";

import Settings from "../../icons/settings-img/settingscolor.svg";
import SettingsColor from "../../icons/settings-img/settings.svg";

import Music from "../../icons/music/Music.png";
import MusicColor from "../../icons/music/MusicColor.png";

import Search from "../../icons/search/Search.png";
import SearchColor from "../../icons/search/SearchColor.png";

import "./Navbar.scss"

const dontShowRouter = ["/", "/login", "/register", "/login/recover", "/loadingbuy", "/ticket/:id/pago"]


const Navbar = (props) => {

    const [active, setActive] = useState("Home")
    if (dontShowRouter.indexOf(props.location.pathname) > -1) return null;
    return (
        <div id="frame">
            <div>
                <div id="menu">
                    <Link to="/ticket">
                        <div id="plus">
                            <img src={active === "Tickets" ? TicketColor : Tickets}
                                alt="" className="efect"
                                onClick={() => setActive('Tickets')}
                            />
                        </div>
                    </Link>
                    <div id="plus"   >
                        <Link to="/search">
                            <img src={active === "Search" ? SearchColor : Search}
                                alt="" className="efect"
                                onClick={() => setActive('Search')}
                            />
                        </Link>
                    </div>
                    <div id="plus"  >
                        <Link to="/home">
                            <img src={active === "Home" ? HomeColor : Home}
                                alt="" className="efect"
                                onClick={() => setActive('Home')}
                            />
                        </Link>
                    </div>
                    <div id="plus" >
                        <Link to="/mytickets">
                            <img src={active === "Music" ? MusicColor : Music}
                                alt="" className="efect"
                                onClick={() => setActive('Music')}
                            />
                        </Link>
                    </div>
                    <Link to="/settings">
                        <div id="plus"  >
                            <img src={active === "Settings" ? SettingsColor : Settings}
                                alt="" className="efect"
                                onClick={() => setActive('Settings')}
                            />
                        </div>
                    </Link>
                </div>
            </div>
        </div>
    )
}


export default withRouter(Navbar);
